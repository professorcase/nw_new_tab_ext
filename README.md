# Semester New Tab Chrome Extenion

Customized Chrome 'New Tab' Extension, updated to reflect current links each semester

Uses Bing Image of the Day for the background. 


## References & Links

- [Dillinger Markdown Editor](http://dillinger.io/)
- [Bing Image URL](http://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1)
- [Stack Overflow Question](https://stackoverflow.com/questions/10639914/is-there-a-way-to-get-bings-photo-of-the-day/45472526#45472526)
- [Clock](
https://www.ricocheting.com/code/javascript/html-generator/date-time-clock)
